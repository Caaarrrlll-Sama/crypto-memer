const puppeteer = require('puppeteer');

async function main() {
    const browser = await puppeteer.launch({
        headless: false,
        args: [
            '--incognito',
            '--allow-running-insecure-content'
        ],
    });

    const page = await browser.newPage();
    //Ethereum farming site
    await page.goto(`http://ethereumfaucet.info/`);

    // await browser.close();
}

main();